﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class Task2
    {
        static void Main(string[] args)
        {
            var sten = new SalariedEmployee("Sten", "Hard", "73403fjh91", 23.5);
            Console.WriteLine($"Sten has a social security number of {sten.SSN} and a weekly salary of {sten.WeeklySalary} of some random currency");

            var bjorn = new HourlyEmployee("Bjorn", "Rik", "fsjdklfd89", 37, 13);
            Console.WriteLine($"Bjorns last name is {bjorn.LastName} and he earned {bjorn.Earning()} this week");

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}

abstract class Employee
{
    private string _firstName, _lastName, _ssn;

    public string FirstName { get => _firstName; }
    public string LastName { get => _lastName; }
    public string SSN { get => _ssn; }

    public Employee(string firstName, string lastName, string ssn)
    {
        _firstName = firstName;
        _lastName = lastName;
        _ssn = ssn;
    }

    public override string ToString()
    {
        return FirstName + " " + LastName + " " + SSN;
    }

    public abstract double Earning();
};

class SalariedEmployee : Employee
{
    private double _weeklySalary;

    public double WeeklySalary { get => _weeklySalary; }
    public SalariedEmployee(string firstName, string lastName, string ssn, double weeklySalary)
        : base(firstName, lastName, ssn)
    {
        _weeklySalary = weeklySalary;
    }

    public override string ToString()
    {
        return base.ToString() + "\n" + $"Weekly Salary: {WeeklySalary}";
    }
    public override double Earning()
    {
        return _weeklySalary;
    }
}

class HourlyEmployee : Employee
{
    private int _weeklyWorkedHours;
    private double _hourlyWage;

    public HourlyEmployee(string firstName,
                          string lastName,
                          string ssn,
                          int weeklyWorkedHours,
                          double hourlyWage)
    : base(firstName, lastName, ssn)
    {
        _weeklyWorkedHours = weeklyWorkedHours;
        _hourlyWage = hourlyWage;
    }

    public int WeeklyWorkedHours { get => _weeklyWorkedHours; }

    public double HourlyWage { get => _hourlyWage; }

    public override string ToString()
    {
        return base.ToString() +
            "\n" + $"Weekly worked hours: {WeeklyWorkedHours}" +
            "\n" + $"Hourly Wage: {HourlyWage}";
    }

    public override double Earning()
    {
        return (double)_weeklyWorkedHours * _hourlyWage;
    }
}