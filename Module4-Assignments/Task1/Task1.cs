﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class Task1
    {
        static void Main(string[] args)
        {
            var shape = new Shape(1, 2);
            Console.WriteLine(shape.Display());

            var circle = new Circle(2, 3, 4);
            Console.WriteLine(circle.Display());

            var cylinder = new Cylinder(3, 4, 5, 6);
            Console.WriteLine(cylinder.Display());

            Console.WriteLine("press any key to exit");
            Console.ReadKey();
        }
    }
}

class Shape
{
    protected int _x;
    protected int _y;

    public Shape(int x, int y)
    {
        _x = x;
        _y = y;
    }

    public int X
    {
        get { return _x; }
    }

    public int Y
    {
        get { return _y; }
    }

    public virtual string Display()
    {
        return $"X: {_x}, Y: {_y}";
    }
};

class Circle : Shape
{
    protected int _r;
    public Circle(int x, int y, int r)
        : base(x, y)
    {
        _r = r;
    }

    public virtual double Area
    {
        get => Math.PI * Math.Pow(_r, 2);
    }

    public override string Display()
    {
        return base.Display() + $", R: {Area}";
    }
};

class Cylinder : Circle
{
    protected int _h;
    public Cylinder(int x, int y, int r, int h) : base(x, y, r)
    {
        _h = h;
    }

    public override double Area
    {
        get => (2 * Math.PI * Math.Pow(_r, 2)) + (2 * Math.PI * _r * _h);
    }

    public virtual double Volume
    {
        get => Math.PI * Math.Pow(_r, 2) * _h;
    }

    public override string Display()
    {
        return base.Display() + $", V: {Volume}";
    }
}