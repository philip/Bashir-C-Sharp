﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    internal class Task5
    {
        static void Main(string[] args)
        {
            Random myRandom = new Random();
            int[] studentGrades = new int[10];

            for (int i = 0; i < 10; i++)
            {
                studentGrades[i] = myRandom.Next(1, 10);
            }

            int k = 1;
            foreach (var grade in studentGrades)
            {
                Console.Write($"Student {k++} has got {grade}");
                for (int l = 0; l < grade; l++)
                {
                    Console.Write(" * ");
                }
                Console.Write("\n");
            }
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
