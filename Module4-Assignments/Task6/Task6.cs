﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    internal class Task6
    {
        static void Main(string[] args)
        {

            //Console.WriteLine(student);
            List<Student> studentsInClass = new List<Student>{
                new Student($"Bashir Nulusson", new DateTime(1987, 8, 17), Course.CSharp, Grade.A),
                new Student($"Andy Johansson", new DateTime(1997, 3, 11), Course.SoftwareDevelopment, Grade.C),
                new Student($"Peter Bondesson", new DateTime(1994, 10, 07), Course.CSharp, Grade.B),
                new Student($"Jack Philsson", new DateTime(1980, 6, 27), Course.SoftwareDevelopment, Grade.A),
                new Student($"Kyagulanyi Ssentamu", new DateTime(1988, 1, 15), Course.SoftwareDevelopment, Grade.B),
                new Student($"Mutebi Kateregga", new DateTime(1990, 9, 22), Course.CSharp, Grade.A),
                new Student($"Wasswa Mufumbiro", new DateTime(1992, 12, 02), Course.CSharp, Grade.C),
        };

            void AddStudent(Student student)
            {
                studentsInClass.Add(student);
            }

            bool CheckIfStudentInList(string studentName)
            {
                return studentsInClass.FindAll(el => el.NameOfStudent == studentName).Any();
            }

            void PrintStudentsInCouser(Course course)
            {
                IEnumerable<Student> fooMatches = studentsInClass.Where(el => el.Course == course);

                Console.WriteLine($"All students in the {course} course");
                foreach (var student in fooMatches)
                {
                    Console.WriteLine($"{student.NameOfStudent} has grade {student.Grade}");
                }
            }

            bool RemoveStudent(string studentName)
            {
                    return studentsInClass.RemoveAll(el => el.NameOfStudent == studentName) > 0;
            }

            void DisplayStudentsInNameOrder()
            {
                Console.WriteLine("List of all enrolled students in the program:");
                IEnumerable<Student> query = from student in studentsInClass
                                            orderby student.NameOfStudent ascending
                                            select student;
                foreach (var student in query)
                {
                    Console.WriteLine(student.NameOfStudent);
                }
            }

            foreach (Student student in studentsInClass)
            {
                Console.WriteLine(student);
            }

            Console.WriteLine();

            
                Console.WriteLine(CheckIfStudentInList("Bashir Nulusson") ? "YEEEES!!!" : "NOOOO");
            
            Console.WriteLine();

            PrintStudentsInCouser(Course.CSharp);
            Console.WriteLine();
            DisplayStudentsInNameOrder();
            Console.WriteLine();


            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}

public enum Course
{
    CSharp,
    SoftwareDevelopment,
}

public enum Grade
{
    A,
    B,
    C,
    E,
    D,
    F,
}

public class Student
{

    public string NameOfStudent { get; }
    public DateTime BirthDate { get; }
    public Course Course { get; }
    public Grade Grade { get; }

    public Student(string nameOfStudent, DateTime birthDate, Course course, Grade grade)
    {
        NameOfStudent = nameOfStudent;
        BirthDate = birthDate;
        Course = course;
        Grade = grade;
    }

    //public override bool Equals(object obj)
    //{
    //    var other = obj as Student;
    //    return other.NameOfStudent == this.NameOfStudent;
    //}

    //public static bool operator ==(Student a, Student b)
    //{
    //    return a.NameOfStudent == b.NameOfStudent;
    //}

    //public static bool operator !=(Student a, Student b)
    //{
    //    return a != b;
    //}

    public override string ToString()
    {
        return $"Name of student: {NameOfStudent}" + "\n" + $"Date of birth: {BirthDate.ToString("yyyy MMMM dd")}"
            + "\n" + $"Course: {Course}" + "\n" + $"Grade: {Grade}";
    }
}

