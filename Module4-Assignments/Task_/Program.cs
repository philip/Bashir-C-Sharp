﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IPayable bill1 = new Invoice("jadajada", 72, 13.49);
            //bill1.GetPaymentAmount();

            IPayable bill2 = new Invoice("mambojambo", 23, 119.99);
            //bill2.GetPaymentAmount();

            IPayable kristianstadStore = new MyTotalIncome("Kristianstad", 100, 32.99);
            
            IPayable malmöStore = new MyTotalIncome("Malmö", 256, 33.50);

            IPayable [] participants = new IPayable [4] { bill1, bill2, kristianstadStore, malmöStore };
            for(int i = 0; i < participants.Length; i++)
            {
                participants[i].GetPaymentAmount();
            }

            foreach ( var x in participants)
            {
                x.GetPaymentAmount();
            }
            
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

        }
    }
}
interface IPayable
{
    double GetPaymentAmount();
}
abstract class Employee : IPayable
{
    private string _firstName, _lastName, _ssn;

    public string FirstName { get => _firstName; }
    public string LastName { get => _lastName; }
    public string SSN { get => _ssn; }

    public Employee(string firstName, string lastName, string ssn)
    {
        _firstName = firstName;
        _lastName = lastName;
        _ssn = ssn;
    }

    public override string ToString()
    {
        return FirstName + " " + LastName + " " + SSN;
    }

    public abstract double GetPaymentAmount();
}
class Invoice : IPayable
{
    private string _invoiceNumber;
    private int _quantity;
    private double _pricePerItem;

    public string InvoiceNumber { get => _invoiceNumber; }
    public int Quantity { get => _quantity; }
    public double PricePerItem { get => _pricePerItem; }

    public Invoice(string invoiceNumber, int quantity, double pricePerItem)
    {
        _invoiceNumber = invoiceNumber;
        _quantity = quantity;
        _pricePerItem = pricePerItem;
    }
    public double GetPaymentAmount()
    {
        double paymentAmount = Quantity * PricePerItem;
        Console.WriteLine("Payment amount is: " + $"{paymentAmount}");
        return paymentAmount;
    }

}
public class MyTotalIncome : IPayable
{
    private string _mystore;
    private int _numberOfSales;
    private double _incomePerSale;

    public string Mystore { get => _mystore; }
    public int NumberOfSales { get => _numberOfSales; }
    public double IncomePerSale { get => _incomePerSale; }

    public MyTotalIncome(string mystore, int numberOfSales, double incomePerSale)
    {
        _mystore = mystore;
        _numberOfSales = numberOfSales;
        _incomePerSale = incomePerSale;
    }
    public double GetPaymentAmount()
    {
        double totalIncome = IncomePerSale * NumberOfSales;
        Console.WriteLine("TotalIncome is: " + totalIncome);
        return totalIncome;
    }
}

