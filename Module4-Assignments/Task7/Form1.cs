﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task7
{
    public partial class Bashir : Form
    {
        public Bashir()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_celsius_Click(object sender, EventArgs e)
        {
            float c = 0;
            c = float.Parse( textBox1.Text );
            float f = (c / (5f / 9f)) + 32;
            textBox2.Text = f.ToString();

        }

        private void btn_fahrenheit_Click(object sender, EventArgs e)
        {
            float f = 0;
            f = float.Parse( textBox2.Text );
            float c = (5f / 9f) * (f - 32);
            textBox1.Text = c.ToString();
        }
    }
}
