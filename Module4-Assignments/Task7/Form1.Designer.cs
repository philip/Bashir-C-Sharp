﻿namespace Task7
{
    partial class Bashir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_celsius = new System.Windows.Forms.Label();
            this.txt_fahrenheit = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btn_celsius = new System.Windows.Forms.Button();
            this.btn_fahrenheit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_celsius
            // 
            this.txt_celsius.AutoSize = true;
            this.txt_celsius.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.txt_celsius.Location = new System.Drawing.Point(282, 113);
            this.txt_celsius.Name = "txt_celsius";
            this.txt_celsius.Size = new System.Drawing.Size(151, 46);
            this.txt_celsius.TabIndex = 0;
            this.txt_celsius.Text = "Celsius";
            // 
            // txt_fahrenheit
            // 
            this.txt_fahrenheit.AutoSize = true;
            this.txt_fahrenheit.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.txt_fahrenheit.Location = new System.Drawing.Point(247, 225);
            this.txt_fahrenheit.Name = "txt_fahrenheit";
            this.txt_fahrenheit.Size = new System.Drawing.Size(209, 46);
            this.txt_fahrenheit.TabIndex = 1;
            this.txt_fahrenheit.Text = "Fahrenheit";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBox1.Location = new System.Drawing.Point(507, 110);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(248, 53);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.textBox2.Location = new System.Drawing.Point(507, 222);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(248, 53);
            this.textBox2.TabIndex = 3;
            // 
            // btn_celsius
            // 
            this.btn_celsius.Location = new System.Drawing.Point(846, 113);
            this.btn_celsius.Name = "btn_celsius";
            this.btn_celsius.Size = new System.Drawing.Size(82, 50);
            this.btn_celsius.TabIndex = 4;
            this.btn_celsius.Text = "Convert";
            this.btn_celsius.UseVisualStyleBackColor = true;
            this.btn_celsius.Click += new System.EventHandler(this.btn_celsius_Click);
            // 
            // btn_fahrenheit
            // 
            this.btn_fahrenheit.Location = new System.Drawing.Point(846, 222);
            this.btn_fahrenheit.Name = "btn_fahrenheit";
            this.btn_fahrenheit.Size = new System.Drawing.Size(82, 53);
            this.btn_fahrenheit.TabIndex = 5;
            this.btn_fahrenheit.Text = "Convert";
            this.btn_fahrenheit.UseVisualStyleBackColor = true;
            this.btn_fahrenheit.Click += new System.EventHandler(this.btn_fahrenheit_Click);
            // 
            // Bashir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1037, 450);
            this.Controls.Add(this.btn_fahrenheit);
            this.Controls.Add(this.btn_celsius);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txt_fahrenheit);
            this.Controls.Add(this.txt_celsius);
            this.Name = "Bashir";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txt_celsius;
        private System.Windows.Forms.Label txt_fahrenheit;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btn_celsius;
        private System.Windows.Forms.Button btn_fahrenheit;
    }
}

