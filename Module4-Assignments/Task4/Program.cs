﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // string[] colllection = { 

            ISpaceObject[] drawables = { new Venusian(), new Martian("", 0, ""), new LaserBeam() };

            bool exit = false;
            while (!exit)
            {
                var key = Console.ReadKey();
                if (key.KeyChar == 'r')
                {
                    foreach (ISpaceObject drawable in drawables)
                    {
                        Console.Write(drawable.Draw());
                    }
                }
                else if (key.KeyChar == 'q')
                {
                    exit = true;
                }


            }
        }
    }
}

interface ISpaceObject
{
    string Draw();


}

public class Arena
{
    public int X { get; set; }
    public int Y { get; set; }

    //! @x size horizontall

    Arena(int x, int y)
    {
        X = x;
        Y = y;
    }
}

abstract public class Position
{
    public int X { get; set; }
    public int Y { get; set; }
    private Arena _arena;

    Position(int x, int y, ref Arena arena)
    {
        X = x;
        Y = y;
        _arena = arena;
    }

    public void Move(int x, int y)
    {
        X += x;
        Y += y;
    }



}

abstract public class SpaceObject
{
    protected string _nameOfObject;
    protected int _idOfObject;

    public string NameOfObject { get { return _nameOfObject; } }
    public int IdOfObject { get { return _idOfObject; } }

    public SpaceObject(string nameOfObject, int idOfObject)
    {
        _nameOfObject = nameOfObject;
        _idOfObject = idOfObject;
    }
    public override string ToString()
    {
        return NameOfObject + "\t" + IdOfObject;
    }
    public abstract string Draw();


}
public class Martian : ISpaceObject
{
    private string _discription;

    public string Discription { get { return _discription; } }
    public Martian(string nameOfObject, int idOfObject, string discription)
    //: base(nameOfObject, idOfObject)
    {
        _discription = discription;
        // _discription = "This space object is coloured red and has 6 antennae";
    }
    public override string ToString()
    {
        return base.ToString() + "\n" + $"{Discription}";
    }
    public string Draw()
    {
        return _discription;
    }
}

public class Venusian : ISpaceObject
{
    public string Draw()
    {
        return "V";
    }
}
public class Plutonian : ISpaceObject
{
    public string Draw()
    {
        return "P";
    }
}
public class LaserBeam : ISpaceObject
{
    public string Draw()
    {
        return "L";
    }
}