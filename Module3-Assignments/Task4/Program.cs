﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            for (; ; )
            {

                Console.WriteLine("Welcome!");
                int currentYear = DateTime.Now.Year;
                Console.WriteLine("State your first name or enter \"q\" to exit");
                var firstName = Console.ReadLine();
                if (firstName == "q")
                    return;
                Console.WriteLine("State your last name: ");
                var lastName = Console.ReadLine();
                if (lastName == "q")
                    return;
                Console.WriteLine("State your year of birth with 4 digits: ");

                int yearOfBirth;

                // Cannot be born in the future or be above 100 years old
                bool validYear(int year)
                {
                    return year > currentYear - 100 && year <= currentYear;
                }

                while (!(int.TryParse(Console.ReadLine(), out yearOfBirth) && validYear(yearOfBirth)))
                {
                    Console.WriteLine("Invalid year of birth try again");
                }            

                var heartRates = new HeartRates(firstName, lastName, yearOfBirth, currentYear);


                Console.WriteLine($"Hello {heartRates.FirstName} {heartRates.LastName}");
                Console.WriteLine($"Your age is {heartRates.age()} and maximum heart rate is {heartRates.maxHeartRate()}");
                Console.WriteLine($"Your target heart rate is between {heartRates.minTargetRange()} and {heartRates.maxTargetRange()}")
            }

        }
    }
    class HeartRates
    {
        private string _firstName;
        private string _lastName;
        private int _yearOfBirth;
        private int _currentYear;
        //private TargetRange _targetRange;
        // private Tuple<int, int> _targetRange2;

        //public struct TargetRange
        //{
        //    private double _min;
        //    private double _max;

        //    public TargetRange(int age)
        //    : this()
        //    {
        //        double maxHeartrate = 220 - (double)age;
        //        _min = maxHeartrate * 0.5;
        //        _max = maxHeartrate * 0.85;
        //    }

        //    public double Min { get; }
        //    public double Max { get; }
        //}

        public HeartRates(string firstName,
                          string lastName,
                          int yearofBirth,
                          int currentYear)
        {
            _firstName = firstName;

            _lastName = lastName;
            _yearOfBirth = yearofBirth;
            _currentYear = currentYear;
            //_targetRange = new TargetRange(currentYear - yearofBirth);
        }
        public string FirstName
        {
            set { _firstName = value; }
            get => _firstName;
        }

        public string LastName
        {
            set { _lastName = value; }
            get => _lastName;
        }

        public int YearOfBirth { set; get; }

        //public TargetRange PersonalTargetRange { get {return _targetRange;} }
        public int age()
        {
            return _currentYear - _yearOfBirth;
        }
        public double maxHeartRate()
        {
            return 220 - (double)age();
        }

        public double minTargetRange()
        {
            return maxHeartRate() * 0.5;
        }
        public double maxTargetRange()
        {
            return maxHeartRate() * 0.85;
        }
    }
}
