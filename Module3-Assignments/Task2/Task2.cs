﻿
using System;
using System.Collections.Generic;

namespace Task2
{
    internal class Task2
    {
        static void Main(string[] args)
        {
            var atm = new ATM();

            Console.WriteLine("Enter Your Account Number");
            var input = Console.ReadLine();

            if (atm.selectAccount(input))
            {
                Console.WriteLine($"Welcome! Your account balance is {atm.getBalance()}");
            }
            else
            {
                Console.WriteLine("The account does not exist, a new one will be created");
                Console.WriteLine("Enter the balance for this account:");
                var validInput = false;
                while (!validInput)
                {
                    try
                    {
                        // Negative balance is OK. Customer may be taking a loan
                        var balance = int.Parse(Console.ReadLine());
                        atm.createAccount(input, balance);
                        validInput = true;
                    }
                    catch
                    {
                        Console.WriteLine("Incorrect balance, try again");
                    }
                }
            }

            var sessionIsActive = true;
            while (sessionIsActive)
            {
                Console.WriteLine("Make your selection:");
                Console.WriteLine("1: Make Deposit");
                Console.WriteLine("2: Withdraw Money");
                Console.WriteLine("3: Check your balance:");
                Console.WriteLine("4: To exit");

                int selection = 0;
                try
                {
                    selection = int.Parse(Console.ReadLine());
                    if (selection < 1 || selection > 4)
                    {
                        Console.WriteLine("Invalid Selection, try again");
                        continue;
                    }

                }
                catch
                {
                    Console.WriteLine("Invalid Selection, try again");
                    continue;
                }

                switch (selection)
                {
                    case 1:
                        Console.WriteLine("How much would you like to deposit?");
                        var depo = int.Parse(Console.ReadLine());
                        atm.deposit(depo);
                        break;
                    case 2:
                        Console.WriteLine("How much would you like to withdraw=");
                        var withdraw = int.Parse(Console.ReadLine());
                        if (!atm.withdraw(withdraw))
                            Console.WriteLine("You don't have enough money!");
                        break;
                    case 3:
                        Console.WriteLine($"Your balance is: {atm.getBalance()}");
                        break;
                    case 4:
                        goto default;
                    default:
                        return;
                }
            }
        }
    }

    class ATM
    {
        class Account
        {
            private double _balance;

            public Account(double accountBalance = 0)
            {
                _balance = accountBalance;
            }

            //! @return the new balance
            public bool deposit(double amount)
            {
                _balance += amount;
                return true;
            }

            //! @return if the withdraw was possible
            public bool withdraw(double amount)
            {
                if (_balance - amount <= 0)
                    return false;

                _balance -= amount;
                return true;
            }

            // Get the current account balance
            public double Balance
            {
                get => _balance;
            }
        }

        private string _selectedAccount;

        private Dictionary<string, Account> _accountRegistry;

        public ATM()
        {
            _accountRegistry = new Dictionary<string, Account> { { "AB123", new Account(123) } };
        }
        public bool selectAccount(string accountNumber)
        {
            if (_accountRegistry.ContainsKey(accountNumber))
            {
                _selectedAccount = accountNumber;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool createAccount(string accountNumber, double balance = 0)
        {
            if (_accountRegistry.ContainsKey(accountNumber))
            {
                Console.WriteLine("This account already exists");
                return false;
            }
            _accountRegistry.Add(accountNumber, new Account(balance));
            selectAccount(accountNumber);
            return true;
        }

        public double getBalance()
        {
            return _accountRegistry[_selectedAccount].Balance;
        }

        public bool withdraw(double amount)
        {
            return _accountRegistry[_selectedAccount].withdraw(amount);
        }

        public bool deposit(double amount)
        {
            return _accountRegistry[_selectedAccount].deposit(amount);
        }

    }
}

