﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{


    internal class Program
    {
        static void Main(string[] args)
        {
            var availableModels = new List<string> { "Camber Comp", "Challenger", "Discovery", "Electric" };
            var availableSizes = new List<string> { "S", "M", "L", "XL" };

            var bikes = new List<Bicycle>();

            var rnd = new Random();
            for (int i = 1; i <= 10; i++)
            {
                var randomFrameSizeListIndex = rnd.Next(availableSizes.Count);
                var randomModelListIndex = rnd.Next(availableModels.Count);

                bikes.Add(new Bicycle(availableModels[randomModelListIndex], availableSizes[randomFrameSizeListIndex]));
                //Console.WriteLine($"The list now has {bikes.Count} members and the last added was {bikes.Last().Id}");
            }

            Console.WriteLine("Bikes have been created");
            printInfo(bikes);


            Console.WriteLine();
            Console.WriteLine("Accelerating...");
            foreach (var item in bikes)
            {
                int speed = rnd.Next(10, 50);
                item.Speed = speed;
            }

            printInfo(bikes);


            while (Console.ReadKey().ToString().Equals("Q"))
            {
                System.Threading.Thread.Sleep(1000);
            }

        }

        static void printInfo(List<Bicycle> bikes)
        {
            const int columnWidth = -20;
            Console.WriteLine("Listing all bicycles:");
            Console.WriteLine($"{"Serial Number",columnWidth}" + $"{"Model",columnWidth}" + $"{"Frame Size",columnWidth}" + $"{"Speed",columnWidth}");
            foreach (var item in bikes)
            {
                Console.WriteLine($"{item.Id,columnWidth}" + $"{item.Model,columnWidth}" + $"{item.Size,columnWidth}" + $"{item.Speed,columnWidth}");
            }
        }

    }
    class Bicycle
    {
        // Variable shared by all intances
        private static int _incrementalId = 1000;
        private int _id;
        private int _speed;
        private string _model;
        private string _size;

        public Bicycle(string model, string frameSize = "M")
        {
            _incrementalId += 1;
            _id = _incrementalId;
            //Console.WriteLine($"Incremental ID is {_id}");
            _speed = 0;
            _model = model;
            _size = frameSize;
        }
        public int Id
        {
            get { return _id; }
        }

        public string Size
        {
            get { return _size; }
        }

        public int Speed
        {
            set
            {
                if (value >= 0)
                    _speed = value;
                else
                    _speed = 0;
            }
            get
            {
                return _speed;
            }
        }
        public string Model
        {
            get => _model;
        }
        public bool accelerate()
        {
            int increase = 5;
            if (this._speed + increase >= 100)
                return false;
            this._speed += increase;
            return true;
        }
        public bool brake()
        {
            int decrease = 5;
            if (this._speed - decrease <= 0)
                return false;
            this._speed -= decrease;
            return true;
        }
    }
}
