﻿using System;
using System.Collections.Generic;

namespace List_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> names = new List<string>();

            Console.WriteLine("Count: {0}", names.Count);

            names.Add("Wera"); names.Add("Sven"); names.Add("Anna"); names.Add("Emilia"); names.Add("Mikael");

            Console.WriteLine();

            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine("Count: {0}", names.Count);

            Console.WriteLine("\nContains(\"Anna\"): {0}", names.Contains("Anna"));

            Console.WriteLine("\nInsert(2, \"Hakan\")");

            names.Insert(2, "Hakan");

            Console.WriteLine();

            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine("\nnames[3]: {0}", names[3]);

            Console.WriteLine("\nRemove(\"Hakan\")");

            names.Remove("Hakan");

            Console.WriteLine();
            
            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine();
            Console.WriteLine("Check this out! The list is sorted");

            //sort the elements in the list
            names.Sort();

            foreach (string name in names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine("Count: {0}", names.Count);

            names.Clear();

            Console.WriteLine("\nClear()");
            Console.WriteLine("Count: {0}", names.Count);
        }

    }
}
