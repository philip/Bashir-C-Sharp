﻿namespace bankApplication
{
    partial class Account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.First_name_Table = new System.Windows.Forms.TextBox();
            this.Account_nr_Table = new System.Windows.Forms.TextBox();
            this.Address_Table = new System.Windows.Forms.TextBox();
            this.Surname_Table = new System.Windows.Forms.TextBox();
            this.Telephone_Table = new System.Windows.Forms.TextBox();
            this.Pin_Table = new System.Windows.Forms.TextBox();
            this.Education_Table = new System.Windows.Forms.ComboBox();
            this.Birth_date_Table = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.Person_number_Table = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1415, 100);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(1371, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 40);
            this.label2.TabIndex = 3;
            this.label2.Text = "X";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(310, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(703, 40);
            this.label1.TabIndex = 2;
            this.label1.Text = "NULUSSON BANK OPERATING SYSTEM";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label5.Location = new System.Drawing.Point(102, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 26);
            this.label5.TabIndex = 10;
            this.label5.Text = "ACCOUNT NUMBER";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label3.Location = new System.Drawing.Point(102, 305);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 26);
            this.label3.TabIndex = 11;
            this.label3.Text = "FIRST NAME";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label4.Location = new System.Drawing.Point(102, 385);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 26);
            this.label4.TabIndex = 12;
            this.label4.Text = "SURNAME";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label6.Location = new System.Drawing.Point(102, 463);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 26);
            this.label6.TabIndex = 13;
            this.label6.Text = "ADDRESS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label8.Location = new System.Drawing.Point(786, 385);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 26);
            this.label8.TabIndex = 15;
            this.label8.Text = "EDUCATION";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label9.Location = new System.Drawing.Point(799, 463);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 26);
            this.label9.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label10.Location = new System.Drawing.Point(786, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(177, 26);
            this.label10.TabIndex = 17;
            this.label10.Text = "DATE OF BIRTH";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label11.Location = new System.Drawing.Point(786, 463);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 26);
            this.label11.TabIndex = 18;
            this.label11.Text = "PIN";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label12.Location = new System.Drawing.Point(786, 305);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 26);
            this.label12.TabIndex = 19;
            this.label12.Text = "TELEPHONE";
            // 
            // First_name_Table
            // 
            this.First_name_Table.AllowDrop = true;
            this.First_name_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.First_name_Table.Location = new System.Drawing.Point(371, 297);
            this.First_name_Table.Name = "First_name_Table";
            this.First_name_Table.Size = new System.Drawing.Size(322, 39);
            this.First_name_Table.TabIndex = 21;
            // 
            // Account_nr_Table
            // 
            this.Account_nr_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Account_nr_Table.Location = new System.Drawing.Point(371, 213);
            this.Account_nr_Table.Name = "Account_nr_Table";
            this.Account_nr_Table.Size = new System.Drawing.Size(322, 39);
            this.Account_nr_Table.TabIndex = 20;
            // 
            // Address_Table
            // 
            this.Address_Table.AllowDrop = true;
            this.Address_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Address_Table.Location = new System.Drawing.Point(371, 455);
            this.Address_Table.Multiline = true;
            this.Address_Table.Name = "Address_Table";
            this.Address_Table.Size = new System.Drawing.Size(322, 117);
            this.Address_Table.TabIndex = 23;
            // 
            // Surname_Table
            // 
            this.Surname_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Surname_Table.Location = new System.Drawing.Point(371, 377);
            this.Surname_Table.Name = "Surname_Table";
            this.Surname_Table.Size = new System.Drawing.Size(322, 39);
            this.Surname_Table.TabIndex = 22;
            // 
            // Telephone_Table
            // 
            this.Telephone_Table.AllowDrop = true;
            this.Telephone_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Telephone_Table.Location = new System.Drawing.Point(999, 297);
            this.Telephone_Table.Name = "Telephone_Table";
            this.Telephone_Table.Size = new System.Drawing.Size(322, 39);
            this.Telephone_Table.TabIndex = 26;
            // 
            // Pin_Table
            // 
            this.Pin_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pin_Table.Location = new System.Drawing.Point(999, 455);
            this.Pin_Table.Name = "Pin_Table";
            this.Pin_Table.Size = new System.Drawing.Size(322, 39);
            this.Pin_Table.TabIndex = 25;
            // 
            // Education_Table
            // 
            this.Education_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Education_Table.FormattingEnabled = true;
            this.Education_Table.Items.AddRange(new object[] {
            "Non Graduate",
            "Undergraduate",
            "Post Graduate"});
            this.Education_Table.Location = new System.Drawing.Point(999, 377);
            this.Education_Table.Name = "Education_Table";
            this.Education_Table.Size = new System.Drawing.Size(322, 40);
            this.Education_Table.TabIndex = 30;
            // 
            // Birth_date_Table
            // 
            this.Birth_date_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Birth_date_Table.Location = new System.Drawing.Point(999, 221);
            this.Birth_date_Table.Name = "Birth_date_Table";
            this.Birth_date_Table.Size = new System.Drawing.Size(322, 28);
            this.Birth_date_Table.TabIndex = 32;
            this.Birth_date_Table.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Window;
            this.button1.Location = new System.Drawing.Point(586, 632);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(235, 44);
            this.button1.TabIndex = 33;
            this.button1.Text = "SUBMIT";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 779);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1415, 17);
            this.panel2.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label13.Location = new System.Drawing.Point(650, 697);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 26);
            this.label13.TabIndex = 35;
            this.label13.Text = "LOG OUT";
            // 
            // Person_number_Table
            // 
            this.Person_number_Table.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Person_number_Table.Location = new System.Drawing.Point(999, 533);
            this.Person_number_Table.Name = "Person_number_Table";
            this.Person_number_Table.Size = new System.Drawing.Size(322, 39);
            this.Person_number_Table.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label7.Location = new System.Drawing.Point(786, 541);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(208, 26);
            this.label7.TabIndex = 36;
            this.label7.Text = "PERSON NUMBER";
            // 
            // Account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1415, 796);
            this.Controls.Add(this.Person_number_Table);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Birth_date_Table);
            this.Controls.Add(this.Education_Table);
            this.Controls.Add(this.Telephone_Table);
            this.Controls.Add(this.Pin_Table);
            this.Controls.Add(this.Address_Table);
            this.Controls.Add(this.Surname_Table);
            this.Controls.Add(this.First_name_Table);
            this.Controls.Add(this.Account_nr_Table);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Account";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox First_name_Table;
        private System.Windows.Forms.TextBox Account_nr_Table;
        private System.Windows.Forms.TextBox Address_Table;
        private System.Windows.Forms.TextBox Surname_Table;
        private System.Windows.Forms.TextBox Telephone_Table;
        private System.Windows.Forms.TextBox Pin_Table;
        private System.Windows.Forms.ComboBox Education_Table;
        private System.Windows.Forms.DateTimePicker Birth_date_Table;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Person_number_Table;
        private System.Windows.Forms.Label label7;
    }
}