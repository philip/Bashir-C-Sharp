﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    internal  class BankLogic
    {
        private List<Customer> _customerList;
        private List<Employee> _employeeList;

        public BankLogic()
        {
            _customerList = new List<Customer>();
            _employeeList = new List<Employee>();
        }

        public List<Customer> GetCustomerList()
        { return _customerList; }
        
        public bool DoesCustomerExist(Customer customer)
        {
            bool exist = false;
            foreach (Customer customer2 in _customerList)
            {
                if (customer2.PersonNumber == customer.PersonNumber)
                {
                    exist = true;
                }
            }
            return exist;
        }
        public bool AddCustomer(Customer customer)
        {
            if (!DoesCustomerExist(customer))
            {
                _customerList.Add(customer);
                return true;
            }
            else
                return false;

        }

        //! @return the employee number
        public int AddEmployee(int pin)
        {
            var employee = new Employee(pin);
            _employeeList.Add(employee);
            return employee.EmploymentNumber;
        }

        public bool EmployeeLogin(int employeeNumber, int pin)
        {
            if (!_employeeList.Any(x => x.EmploymentNumber == employeeNumber))
            {
                Console.WriteLine("Employee does not exist");
                return false;
            }

            var employee = _employeeList.Find(x => x.EmploymentNumber == employeeNumber);
            if (employee.LoginVerification(pin))
                return true;
            else
                Console.WriteLine("Wrong PIN");
            return false;
        }


        //}
        //public bool RemoveCustomer (int PersonNumber )
        //{ 
        //    if ()
        //    return _customerList.Remove(customer); }
        //List<>

        //public List<Customer> RemovedCustomers(double PersonNumber)
        //{
        //    studentsInClass.RemoveAll(el => el.NameOfStudent == studentName) > 0;
        //    if ( Customer.RemoveAll(el => el.NameOfStudent == studentName) > 0;)
        //}


    }
}
