﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    internal class Customer
    {
        public struct ClosingData
        {
            public double Balance;
            public double Interest;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonNumber { get; }
        public string Address { get; set; }
        public List<Account> ListOfAccounts { get; } = new List<Account>();
        public string Occupation { get; set; }
        

        public Customer (string firstname,
                         string lastname,
                         int personNumber,
                         string address,
                         string occupation)
        {
            FirstName = firstname;
            LastName = lastname;
            PersonNumber = personNumber;
            Address = address;
            Occupation = occupation;
        }

        ~Customer()
        {
            // This code here will run when the customer is deleted
        }

        public void AddAccount(Account account)
        {
            ListOfAccounts.Add(account);
        }

        bool RemoveAllAccounts(out List<ClosingData> dataList)
        {
            dataList = new List<ClosingData> ();

            foreach (var account in ListOfAccounts)
            {
                ClosingData data;
                RemoveAccount(account.AccountNumber, out data);
                dataList.Add(data);
            }
            // This will now remove no elements as there should be no left
            ListOfAccounts.Clear(); // This will destory everything without printint any info
            return true;
        }

        bool RemoveAccount(int accountNumber, out ClosingData data)
        {
            data.Interest = 0.0;
            data.Balance = 0.0;
            foreach (var account in ListOfAccounts)
            {
                if (account.AccountNumber == accountNumber)
                {
                    var a = account as CreditAccount;
                    if (a != null)
                    {
                        // Yeah! this is a credit account
                        //data.Debt = bla bla
                    }
                    
                    data.Balance = account.Balance;
                    data.Interest = account.Balance * account.InterestRate;
                    
                    ListOfAccounts.Remove(account);
                }
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return "Name: " + FirstName + " " + LastName + "\n" +
                   "PersonNumber: " + PersonNumber + "\n" +
                   "Address: " + Address + "\n" +
                   "Occupation: " + Occupation;
        }

        //ClosingData data;
        //if (RemoveAccount(accountNumber, data))
    }
}
