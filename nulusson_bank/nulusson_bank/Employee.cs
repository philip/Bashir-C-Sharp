﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    internal class Employee
    {
        static int _employeeNumberIndex = 0;

        public int EmploymentNumber { get;  }

        private int _pin;

        public bool LoginVerification(int pin)
        {
            return _pin == pin;
        }

        public Employee(int pin)
        {
            EmploymentNumber = _employeeNumberIndex++;
            _pin = pin;
        }

        public bool ChangePin(int oldPin, int newPin)
        {
            if (oldPin == _pin)
            {
                _pin = newPin;
                return true;
            }
            return false;
        }
    }
}
