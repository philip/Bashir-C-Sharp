﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    internal abstract class Account //: IAccount
    {
        
        public int AccountNumber { get; internal set; }
        public TypeOfAccount AccountType { get; set; }
        public double InterestRate { get; set; }
        public double Balance { get; set; }

        public List<Transaction> Transactions { get; private set; }

        public Account(double balance = 0.0, double interestRate = 0.01)
        {
            AccountType = TypeOfAccount.None;
            InterestRate = interestRate;
            Balance = balance;
        }
        public override string ToString()
        {
            return $"SavingsAccount Number: {AccountNumber}" + "\t" + $"AccountType: {AccountType}" + "\n" + 
                   $"Interest rate: {InterestRate}" + "\n" + $"Balance: {Balance}";
        }

        //! @return          True if the withdraw was possible
        //!                  False if there aws not enough money or credit
        //! @param amount    The desired amount to witdraw
        abstract public bool Withdraw(double amount);

        public void Deposit(double amount)
        {
            Balance += amount;
            Transactions.Add(
                new Transaction(AccountNumber,
                                Transaction.Type.Deposit,
                                DateTime.Now,
                                amount,
                                Balance));
        }
    }

    internal class SavingsAccount : Account
    {
        private static int _savingsAccountNumberIndex = 1000;

        public SavingsAccount(double balance = 0.0, double interestRate = 0.01)
        : base(balance, interestRate)
        {
            AccountNumber = _savingsAccountNumberIndex++;
            AccountType = TypeOfAccount.SavingsAccount;
        }

        public override bool Withdraw(double amount)
        {
            if (amount > Balance)
                return false;

            Balance -= amount;
            Transactions.Add(
                new Transaction(AccountNumber,
                                Transaction.Type.Withdraw,
                                DateTime.Now,
                                amount,
                                Balance));
            return true;
        }



    }
}
