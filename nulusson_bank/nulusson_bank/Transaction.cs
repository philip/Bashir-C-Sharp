﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    internal class Transaction
    {
        public enum Type
        {
            Withdraw,
            Deposit,
        }
        /*
         * • kontoId (kontot som transaktionen gäller)
        • datum och klockslag
        • transaktionstyp (uttag/insättning)
        • belopp
        • saldo (efter transaktionen)
         */
        public int AccountNumber { get; }
        public Type TransactionType { get; }
        public DateTime TimeOfTransaction { get; }
        public double Amount { get; }
        public double Balance { get; }

        public Transaction(int accountNumber, Type transactionType, DateTime timeOfTransaction, double amount, double balance)
        {
            AccountNumber = accountNumber;
            TransactionType = transactionType;
            TimeOfTransaction = timeOfTransaction;
            Amount = amount;
            Balance = balance;
        }

        public override string ToString()
        {
            return $"{AccountNumber, 20}, " +
                   $"{TransactionType}" +
                   $"{TimeOfTransaction.ToString("dddd dd MMMM HH:MM")}" +
                   $"{Amount}" +
                   $"{Balance}";
        }
    }
}
