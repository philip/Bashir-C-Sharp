﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    //interface IAccount
    //{
    //    int AccountNumber { get; }
    //    TypeOfAccount AccountType { get; }
    //    double Balance { get; }
    //    double InterestRate { get; set; }
    //}

    internal class CreditAccount : Account
    {
        private static int _creditAccountNumberIndex = 1000000;

        public double CreditLimit { get; set ; }
        public double InterestOnLoans { get; set; }

        public CreditAccount(double balance = 0.0,
                             double interestRate = 0.005,
                             double interestOnLoans = 0.07,
                             double creditLimit = 5000)
        : base(balance, interestRate)
        {
            AccountNumber = _creditAccountNumberIndex++;
            AccountType = TypeOfAccount.CreditAccount;
            CreditLimit = creditLimit;
            InterestOnLoans = interestOnLoans;
        }
        public override string ToString()
        {
            return $"Credit Account Number: {AccountNumber}" + "\t" + $"AccountType: {AccountType}" + "\n" + $"Interest rate: {InterestRate}" + "\t" + $"Interest on Loans: {InterestOnLoans}" + "\n" + $"Balance: {Balance}";
        }

        public override bool Withdraw(double amount)
        {
            if (amount > Balance + CreditLimit)
                return false;

            Balance -= amount;
            Transactions.Add(
                new Transaction(AccountNumber,
                                Transaction.Type.Withdraw,
                                DateTime.Now,
                                amount,
                                Balance));
            return true;
        }
    }

    //public IAccount CreateAccountNumber(TypeOfAccount typeOfAccount, double balance)
    //{
    //    if (typeOfAccount == TypeOfAccount.SavingsAccount)
    //    {
    //        return new SavingsAccount(balance);
    //    }
    //    else
    //    {
    //        return new CreditAccount()
    //    }
    //}
}
