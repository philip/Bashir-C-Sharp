﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    public enum TypeOfAccount
    {
        SavingsAccount,
        CreditAccount,
        None,
    }
}
