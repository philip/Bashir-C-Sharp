﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nulusson_bank
{
    internal class Program
    {
        static void Main(string[] args)
        {
            BankLogic myBank = new BankLogic();
            int selection;



            // Some preparations
            myBank.AddEmployee(1234); // employeeNr 0

            var bashir = new Customer("Bashir", "Nulusson", 19980224, "The street Nr2", "Mr");
            bashir.AddAccount(new SavingsAccount(999999));
            myBank.AddCustomer(bashir);





            //Console.WriteLine("Hello, Please select a service from our list below" + "\n" +
            //                   "1. Register as new customer" + "\n" +
            //                   "2. Check if customer exists" + "\n" +
            //                   "3. View Customer List" + "\n" +
            //                   "4. Balance" + "\n" +
            //                   "5. Deposit" + "\n" +
            //                   "6. Withdraw" + "\n" +
            //                   "7. Add account" + "\n" +
            //                   "8. Remove account \n" +
            //                   "9: Exit");
            bool MainMenu()
            {
                Console.WriteLine("Hello! Welcome to the bank. Select one of the two options:\n" +
                                  "1: Employee\n" +
                                  "2: Customer\n" +
                                  "0: Exit");

                //char input = Console.ReadKey().KeyChar;
                //var inputStr = input.ToString();
                //var parseOk = int.TryParse(inputStr, out selection);

                if (int.TryParse(Console.ReadKey().KeyChar.ToString(), out selection))
                //&& Helpers.NumberInRange(selection, 0, 2))
                {
                    switch (selection)
                    {
                        case 1:
                            EmployeeLoginMenu();
                            break;
                        case 2:
                            CustomerLoginMenu();
                            break;
                        case 0:
                            return false; // Exit the program
                        default:
                            Helpers.PresentInvalid("Invalid input");
                            break;
                    }
                }
                Console.Clear();
                return true;
            }

            void CustomerMenu(Customer customer)
            {
                Console.WriteLine($"Hello {customer.FirstName} {customer.LastName}!");
                Console.WriteLine("Please select an account");
                //int i = 0;
                //foreach (var account in customer.ListOfAccounts)
                for (int i = 0; i < customer.ListOfAccounts.Count; i++)
                {
                    //Console.WriteLine($"{i}: {account.AccountNumber} with balance {account.Balance}");
                    Console.WriteLine($"{i}: {customer.ListOfAccounts[i].AccountNumber} with balance {customer.ListOfAccounts[i].Balance}");
                }
                Console.WriteLine();
                bool ok = false;
                while (!ok)
                {
                    int accountIndex;
                    if (!int.TryParse(Console.ReadKey().KeyChar.ToString(), out accountIndex)
                        || accountIndex > customer.ListOfAccounts.Count)
                    {
                        Console.WriteLine("Invalid input, try again");
                        continue;
                    }

                    var account = customer.ListOfAccounts[accountIndex];


                    var menu = new List<string>{
                        "The Employee Menu!!",
                        $"This is the information for the selected account:",
                        account.ToString(),
                        "",
                        "Select what to do in this account ",
                        "1: Witdraw",
                        "2: Deposit",
                        "3: Balance",
                        "4: Add account",
                        "5: Remove account",
                        "0: Logout"
                    };

                    var choice = Helpers.SelectChoice(menu, 0, 5);

                    switch (choice)
                    {
                        case 1:

                        case 2:
                        //break;
                        case 3:

                        case 4:
                        case 5:
                        case 0:
                        default:
                            return;
                    }


                }
            }

            void CustomerLoginMenu()
            {
                Console.Clear();
                Console.WriteLine("Enter your Personal Number");
                int prNr;
                while (!int.TryParse(Console.ReadLine(), out prNr)) { }
                Console.WriteLine("Now enter your pin");
                int pin;
                while (!int.TryParse(Console.ReadLine(), out pin)) { }
                if (myBank.GetCustomerList().Any(x => x.PersonNumber == prNr))
                {
                    var customer = myBank.GetCustomerList().Find(x => x.PersonNumber == prNr);
                    // TODO Add pin to customer.
                    CustomerMenu(customer);
                }
                else
                {
                    Helpers.PresentInvalid("No customer with that PrNr");
                }
            }

            bool EmployeeMenu()
            {
                var menu = new List<string>{
                    "The Employee Menu!!",
                    "1: Attend to customer",
                    "2: Check if customer exists",
                    "3: Register new customer",
                    "0: Exit",

                };

                Console.Clear();
                var employeeChoice = Helpers.SelectChoice(menu, 0, 3);

                switch (employeeChoice)
                {
                    case 1:
                        while (AttendCustomerMenu()) { }
                        break;

                    case 2:
                        Console.WriteLine("Write the personalnumber of the customer to be checked");
                        int personNumberChecked;
                        while (!int.TryParse(Console.ReadLine(), out personNumberChecked)) { }
                        List<Customer> _customerlist2 = myBank.GetCustomerList();
                        bool exist = false;
                        foreach (Customer customer3 in _customerlist2)
                        {
                            if (customer3.PersonNumber == personNumberChecked)
                            {
                                Console.WriteLine($"Name: " + customer3.FirstName + " " + customer3.LastName +
                                                   "\n PersonNumber: " + customer3.PersonNumber +
                                                   "\n Address: " + customer3.Address +
                                                   "\n Occupation: " + customer3.Occupation);
                                exist = true;
                            }
                            else
                                Console.WriteLine($"No person with that personalnumber is listed");
                            exist = true;
                        }
                        Helpers.PresentInvalid();
                        break;
                    case 3:
                        RegisterNewcustomer();
                        break;
                    case 0:
                        return false;
                    default:
                        Helpers.PresentInvalid("Invalid input");
                        break;
                }
                Console.Clear();
                return true;

            }

            void EmployeeLoginMenu()
            {
                Console.Clear();
                Console.WriteLine("Enter your Employee Number");
                int employeeNr;
                while (!int.TryParse(Console.ReadLine(), out employeeNr)) { }
                Console.WriteLine("Now enter your pin");
                int pin;
                while (!int.TryParse(Console.ReadLine(), out pin)) { }
                if (myBank.EmployeeLogin(employeeNr, pin))
                    while (EmployeeMenu()) { }
                else
                    Helpers.PresentInvalid();
            }

            bool DeleteCustomerMenu(Customer customer)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Are you sure? <Y/N>");
                char answer = char.ToUpper(Console.ReadKey().KeyChar);
                if (answer == 'Y')
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Current state of accounts under this person");
                    foreach (var a in customer.ListOfAccounts)
                    {
                        Console.WriteLine(a.ToString());
                        var saving = a as SavingsAccount;
                        if (saving != null)
                        {
                            var balance = saving.Balance;
                            var interestRate = saving.InterestRate;

                            var interest = balance * interestRate;
                            Console.WriteLine($"The Interest on this account is {interest}");
                        }
                        var credit = a as CreditAccount;
                        if (credit != null)
                        {
                            var balance = credit.Balance;

                            if (balance >= 0)
                            {
                                var interestRate = credit.InterestRate;
                                var interest = balance * interestRate;
                                Console.WriteLine($"The Interest on this account is {interest}");
                            }
                            else
                            {
                                var interestOnLoan = credit.InterestOnLoans;
                                var interest = -balance * interestOnLoan;
                                Console.WriteLine($"The Interest on loans (pay back) on this account is {interest}");
                            }
                        }

                        // TODO calculate stuff
                    }
                    Console.WriteLine($"All accounts under the name {customer.FirstName} {customer.LastName} have been deleted");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    myBank.GetCustomerList().Remove(customer);
                }
                else
                {
                    Console.WriteLine("some text!!");
                    return false;
                }
                return true;
            }

            bool AttendCustomerMenu()
            {
                Console.Clear();
                Console.WriteLine("Enter the number of the customer you want to attend");
                Console.WriteLine("Press 0 to go back to previous menu");
                List<Customer> customerlist = myBank.GetCustomerList();
                int i = 1;
                foreach (Customer customer in customerlist)
                {
                    Console.WriteLine($"Customer nr: {i}");
                    Console.WriteLine(customer.ToString());
                    Console.WriteLine();
                }

                int index;
                while (!int.TryParse(Console.ReadLine(), out index))
                {
                    Console.WriteLine("Invalid input, try again:");
                }

                if (index == 0)
                    return false;
                var c = customerlist[index - 1];

                var accountmenu = new List<string>
                        {
                            "The selected customer is: ",
                            c.ToString(),
                            "",
                            "Select what to do with this customer ",
                            "1: Delete customer ",
                            "2: Add account to customer",
                            "3: Remove account",
                            "0: Exit",
                        };

                var accountMenuSelection = Helpers.SelectChoice(accountmenu, 0, 3);
                switch (accountMenuSelection)
                {
                    case 1:

                        DeleteCustomerMenu(c);
                        break;
                    case 2:
                        AddAccountToCustomer(c);
                        break;
                    case 3:
                        Console.WriteLine("Write the account number of the account you intend to remove: ");
                        Console.WriteLine();
                        int accountToRemove;
                        
                        bool validInput2 = false;
                        while (!validInput2)
                        {
                            while (!int.TryParse(Console.ReadLine(), out accountToRemove))
                            {
                                Console.WriteLine("Invalid input, try again");
                            }
                            var itemToRemove = c.ListOfAccounts.Single(r => r.AccountNumber == accountToRemove);
                            if (itemToRemove != null)
                            {
                                Console.WriteLine();
                                Console.WriteLine($"Are you sure you want to remove this account: {accountToRemove} with the following details: ");  
                                Console.WriteLine();
                                accountToRemove.ToString();
                                Console.WriteLine();
                                Console.WriteLine($"Y for yes or N for no");
                                char answerconfirm = char.ToUpper(Console.ReadKey().KeyChar);
                                if (answerconfirm == 'Y')
                                {
                                    Console.WriteLine();
                                    c.ListOfAccounts.Remove(itemToRemove);
                                    Console.WriteLine("Account removed!");
                                    validInput2 = true;
                                }
                                else if (answerconfirm == 'N')
                                {
                                    Console.WriteLine();
                                    Console.WriteLine($"Removal of account {accountToRemove} is terminated");
                                    validInput2 = true;
                                }
                            }
                            else
                            {
                                Helpers.PresentInvalid("The account does not exist or does not belong to this customer");
                                validInput2 |= true;
                            }
                        }
                        break;
                        
                    case 0:
                        return false;

                }
                return true;
            }
            void RegisterNewcustomer()
            {
                Console.WriteLine($"Please state your first name:  ");
                string firstNameCreated = Console.ReadLine();
                Console.WriteLine($"Please state your Surname:  ");
                string secondNameCreated = Console.ReadLine();
                Console.WriteLine("Please state your personNumber: ");
                int personNumberCreated;
                while (!int.TryParse(Console.ReadLine(), out personNumberCreated)) { }
                Console.WriteLine("Please state your address (as one line): ");
                string addressCreated = Console.ReadLine();
                Console.WriteLine("Please state your occupation: ");
                string occupationCreated = Console.ReadLine();

                Console.WriteLine();
                Console.WriteLine($"Name: {firstNameCreated} {secondNameCreated}" + "\n" +
                                  $"PersonNumber: {personNumberCreated}" + "\n" +
                                  $"Address: {addressCreated}" + "\n" +
                                  $"Occupation: {occupationCreated}");

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Is this the information you have entered? (Y for yes/N for no)");
                char confirmCreation = char.ToUpper(Console.ReadKey().KeyChar);
                if (confirmCreation == 'Y')
                {
                    Console.WriteLine();
                    var customerCreated = new Customer(firstNameCreated, secondNameCreated, personNumberCreated, addressCreated, occupationCreated);

                    myBank.AddCustomer(customerCreated);
                    Console.WriteLine("Account added!");
                }
                else
                {
                    Console.WriteLine("Registration terminated!");
                }
            }
            void AddAccountToCustomer(Customer c)
            {
                Console.WriteLine("Select the account type: S for SavingsAcccount or C for CreditAccount");

                bool validInput = false;
                while (!validInput)
                {
                    char accountTypeToCreate = char.ToUpper(Console.ReadKey().KeyChar);
                    if (accountTypeToCreate == 'S')
                    {
                        Console.WriteLine();
                        Console.WriteLine("SavingsAccount has been selected!");
                        var account = new SavingsAccount();
                        c.AddAccount(account);
                        Console.WriteLine("The current account has been added:");
                        Console.WriteLine(account.ToString());
                        validInput = true;
                    }
                    else if (accountTypeToCreate == 'C')
                    {
                        Console.WriteLine();
                        Console.WriteLine("CreditAccount has been selected!");
                        var account = new CreditAccount();
                        c.AddAccount(account);
                        Console.WriteLine("The current account has been added:");
                        Console.WriteLine(account.ToString());
                        validInput = true;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input, try again..");
                    }
                }
            }

            while (MainMenu()) { }


        }
    }

    public class Helpers
    {
        public static bool NumberInRange(int number, int lower, int higher)
        {
            return number >= lower && number <= higher;
        }

        public static void PresentInvalid(string msg = "")
        {
            Console.WriteLine();
            if (msg.Length > 0)
                Console.WriteLine(msg);
            Console.WriteLine($"try again in 5s");
            for (int i = 0; i < 10; i++)
            {
                Console.Write(".");
                Task.Delay(5000 / 10).Wait();
            }
        }

        public static int SelectChoice(List<string> menuText, int minChoice, int maxChoice)
        {
            int select;
            while (true)
            {
                foreach (string line in menuText)
                {
                    Console.WriteLine(line);
                }

                if (int.TryParse(Console.ReadKey().KeyChar.ToString(), out select)
                    && NumberInRange(select, minChoice, maxChoice))
                {
                    return select;
                }
                Helpers.PresentInvalid("Invalid choice");

                Console.Clear();
            }
        }
    }
}
