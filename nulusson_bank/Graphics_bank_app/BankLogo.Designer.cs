﻿namespace Graphics_bank_app
{
    partial class BankLogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BankLogo));
            this.nulusson_bank_appDataSet = new Graphics_bank_app.nulusson_bank_appDataSet();
            this.account_TableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.account_TableTableAdapter = new Graphics_bank_app.nulusson_bank_appDataSetTableAdapters.Account_TableTableAdapter();
            this.tableAdapterManager = new Graphics_bank_app.nulusson_bank_appDataSetTableAdapters.TableAdapterManager();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.myProgress = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nulusson_bank_appDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_TableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // nulusson_bank_appDataSet
            // 
            this.nulusson_bank_appDataSet.DataSetName = "nulusson_bank_appDataSet";
            this.nulusson_bank_appDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // account_TableBindingSource
            // 
            this.account_TableBindingSource.DataMember = "Account_Table";
            this.account_TableBindingSource.DataSource = this.nulusson_bank_appDataSet;
            // 
            // account_TableTableAdapter
            // 
            this.account_TableTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.Account_TableTableAdapter = this.account_TableTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Transaction_TableTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Graphics_bank_app.nulusson_bank_appDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Green;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(204, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(397, 52);
            this.label1.TabIndex = 1;
            this.label1.Text = "NULUSSON BANK";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(244, 132);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(292, 208);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // myProgress
            // 
            this.myProgress.BackColor = System.Drawing.Color.Green;
            this.myProgress.Location = new System.Drawing.Point(1, 437);
            this.myProgress.Name = "myProgress";
            this.myProgress.Size = new System.Drawing.Size(800, 17);
            this.myProgress.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Green;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(359, 360);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 46);
            this.label2.TabIndex = 4;
            this.label2.Text = "%";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(800, 455);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.myProgress);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nulusson_bank_appDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.account_TableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private nulusson_bank_appDataSet nulusson_bank_appDataSet;
        private System.Windows.Forms.BindingSource account_TableBindingSource;
        private nulusson_bank_appDataSetTableAdapters.Account_TableTableAdapter account_TableTableAdapter;
        private nulusson_bank_appDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar myProgress;
        private System.Windows.Forms.Label label2;
    }
}

