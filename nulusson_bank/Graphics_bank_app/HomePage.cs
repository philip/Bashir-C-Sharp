﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphics_bank_app
{
    public partial class HomePage : Form
    {
        public HomePage()
        {
            InitializeComponent();
        }

        private void account_TableBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.account_TableBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.nulusson_bank_appDataSet);

        }

        private void HomePage_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nulusson_bank_appDataSet.Account_Table' table. You can move, or remove it, as needed.
            this.account_TableTableAdapter.Fill(this.nulusson_bank_appDataSet.Account_Table);

        }
    }
}
