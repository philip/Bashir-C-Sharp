﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Task1();
            Task2();
            Task3();
            Task4();
            Task5();
            Task6();
            Task7();
            Task8();


        }
        public static void Task1()
        {
            //Concatenating two strings (from user input) together
            Console.WriteLine("Please state your name as a full sentence");
            string myName = Console.ReadLine();
            Console.WriteLine("state your place of residence as a full sentence");
            string myResidence = Console.ReadLine();
            Console.WriteLine("Here is what you have stated: {0} " + "{1}", myName, myResidence);
        }
        public static void Task2()
        {
            //Determining whether a number is odd or even
            Console.WriteLine("Please select an integer of your choice.");
            int myInteger = int.Parse(Console.ReadLine());
            if (myInteger % 2 == 0)
            {
                Console.WriteLine("Your selected integer is an even number.");
            }
            else
            {
                Console.WriteLine("Your selected integer is an odd number.");
            }
        }
        public static void Task3()
        {
            //Displaying numbers provided via keyboard on screen with escape sequence \t
            Console.WriteLine("Please state a number between 1 and 50");
            int firstNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Select a second number in the same range");
            int secondNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Select a third in the same range");
            int thirdNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("The numbers selected are: {0} \t{1} \t{2}", firstNumber, secondNumber, thirdNumber);
        }
        public static void Task4()
        {
            Console.WriteLine("My name is Bashir Nulusson. What is the initial of my first name?");
            string firstinitial = Console.ReadLine();
            Console.WriteLine("     {0}{1}{2}", firstinitial, firstinitial, firstinitial);
            Console.WriteLine("     {0}  {1}", firstinitial, firstinitial);
            Console.WriteLine("     {0}{1}{2}", firstinitial, firstinitial, firstinitial);
            Console.WriteLine("     {0}  {1}", firstinitial, firstinitial);
            Console.WriteLine("     {0}{1}{2}", firstinitial, firstinitial, firstinitial);
            Console.WriteLine("What is the initial of my second name?");
            string secondinitial = Console.ReadLine();
            Console.WriteLine("     {0}   {1}", secondinitial, secondinitial);
            Console.WriteLine("     {0}{1}  {2}", secondinitial, secondinitial, secondinitial);
            Console.WriteLine("     {0} {1} {2}", secondinitial, secondinitial, secondinitial);
            Console.WriteLine("     {0}  {1}{2}", secondinitial, secondinitial, secondinitial);
            Console.WriteLine("     {0}   {1}", secondinitial, secondinitial);
        }
        public static void Task5()
        {
            //Adding, substracting, dividing, multiplying, and calculating the remainder 
            Console.WriteLine("Please select an integer");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("Select a second integer");
            int y = int.Parse(Console.ReadLine());
            Console.WriteLine("The sum of {0} and {1} is {2}", x, y, x + y);
            Console.WriteLine("The difference between {0} and {1} is {2}", x, y, x - y);
            Console.WriteLine("The product of {0} and {1} is {2}", x, y, x * y);
            Console.WriteLine("{0} divided by {1} is {2}", x, y, x / y);
            Console.WriteLine("The reminder after dividing {0} by {1} is {2}", x, y, x % y);
        }
        public static void Task6()
        {
            //Calculating the diameter, circumference and area of a circle,given the radius
            double diameterOfcircle = 0, circumferenceOfcircle = 0, areaOfcircle = 0;
            Console.WriteLine("State a radius of a circle");
            double circleRadius = double.Parse(Console.ReadLine());
            diameterOfcircle = 2 * circleRadius;
            circumferenceOfcircle = 2 * (Math.PI) * circleRadius;
            areaOfcircle = (Math.PI) * circleRadius * circleRadius;
            Console.WriteLine("Based on the stated radius, the diameter is {0}, the circumference is {1} and the area is {2}", diameterOfcircle, circumferenceOfcircle, areaOfcircle);
        }
        public static void Task7()
        {
            //Displaying digits separated from each other by a tab.
            int lastDigit = 0, thirdDigit = 0, secondDigit = 0, firstDigit = 0, remainder1 = 0, remainder2 = 0;
            Console.WriteLine("Write a four-digit integer");
            int theNumber = int.Parse(Console.ReadLine());
            lastDigit = theNumber % 10;
            remainder1 = theNumber / 10;
            thirdDigit = remainder1 % 10;
            remainder2 = remainder1 / 10;
            secondDigit = remainder2 % 10;
            firstDigit = theNumber / 1000;
            Console.WriteLine("{0} \t{1} \t{2} \t{3}", firstDigit, secondDigit, thirdDigit, lastDigit);
        }
        public static void Task8()
        {
            //The square and cube of an integer read from the keyboard
            int squareOfnumber = 0, cubeOfnumber = 0;
            Console.WriteLine("Please select an integer of choice");
            int selectedInt = int.Parse(Console.ReadLine());
            squareOfnumber = (int)Math.Pow(selectedInt, 2);
            cubeOfnumber = (int)Math.Pow(selectedInt, 3);
            Console.WriteLine("number \tsquare \tcube" + "\n{0} \t{1} \t{2}", selectedInt, squareOfnumber, cubeOfnumber);
        }
    }
}