class Helpers
{
    public static int getInputNumber()
    {
        int numericInput = 0;
        bool validInput = false;
        string textInput;

        while (!validInput)
        {
            textInput = Console.ReadLine();
            try
            {
                numericInput = Int32.Parse(textInput);
                validInput = true;
                Console.WriteLine($"You have entered the number {textInput}");
            }
            catch (FormatException)
            {
                Console.WriteLine($"Unable to parse '{getInputNumber}' as a number");
            }
        }
        return numericInput;
    }
}