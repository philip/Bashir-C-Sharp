﻿static void ass3()
{
    Console.WriteLine("Enter a number between 2 and 9 for square creation");
    bool correntSize = false;
    int squareSize = 0;

    while (!correntSize)
    {
        squareSize = Helpers.getInputNumber();
        correntSize = squareSize >= 2 && squareSize <= 9;
        Console.WriteLine(correntSize ? $"You have entered size {squareSize}" : $"Size out of bound, try again");
    }


    for (int i = 0; i < squareSize; i++)
    {
        for (int j = 0; j < squareSize; j++)
        {
            Console.Write("*");
        }
        Console.Write("\n");
    }
}

Console.WriteLine();
Console.WriteLine("Assignment 3");
ass3();