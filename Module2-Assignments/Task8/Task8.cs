﻿
public struct ComplexNumber
{
    public int real;
    public int imaginary;

    public ComplexNumber(int real, int imaginary)
    {
        this.real = real;
        this.imaginary = imaginary;
    }

    public static ComplexNumber operator +(ComplexNumber a, ComplexNumber b)
    {
        return new ComplexNumber(a.real + b.real, a.imaginary + b.imaginary);
    }

    public override string ToString()
    {
        return this.real.ToString() + "+ i" + this.imaginary.ToString();
    }
}
public class MethodOverloading
{
    public static int myMethod(int a, int b)
    {
        return a + b;
    }

    public static double myMethod(double a, double b)
    {
        return a + b;
    }

    public static String myMethod(String a, String b)
    {
        return a + " " + b;
    }

    public static ComplexNumber myMethod(ComplexNumber a, ComplexNumber b)
    {
        return a + b;
    }
    static void Main(string[] args)
    {
        bool success = false;

        while (!success)
        {
            try
            {
                Console.WriteLine("Enter an integer and press Enter key: ");
                int a = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Enter an integer and press Enter key: ");
                int b = Int32.Parse(Console.ReadLine());
                Console.WriteLine($"The sum of the two integers is {myMethod(a, b)}");

                Console.WriteLine("Enter a float number and press Enter key: ");
                double da = Double.Parse(Console.ReadLine());
                Console.WriteLine("Enter a float number and press Enter key: ");
                double db = Double.Parse(Console.ReadLine());
                Console.WriteLine($"The sum of the two floats is {myMethod(da, db)}");

                Console.WriteLine("Enter your last name and press Enter key: ");
                string sa = Console.ReadLine();
                Console.WriteLine("Enter your first name and press Enter key: ");
                string sb = Console.ReadLine();
                Console.WriteLine($"Your name is {myMethod(sa, sb)}");

                Console.WriteLine("Enter the real part of the first complex number and press Enter key: ");
                int car = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Enter the imaginary part of the first complex number and press Enter key: ");
                int cai = Int32.Parse(Console.ReadLine());
                ComplexNumber ca = new ComplexNumber(car, cai);
                Console.WriteLine("Enter the real part of the second complex number and press Enter key: ");
                int cbr = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Enter the imaginary part of the second complex number and press Enter key: ");
                int cbi = Int32.Parse(Console.ReadLine());
                ComplexNumber cb = new ComplexNumber(cbr, cbi);
                Console.WriteLine($"The sum of the two complex numbers is {myMethod(ca, cb)}");

                success = true;
            }
            catch
            {
                Console.WriteLine("Parse error, try again...");
            }
        }
    }
}
