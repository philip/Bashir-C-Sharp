﻿static void ass6()
{
    Dictionary<String, String> Grades = new Dictionary<String, String> {
        {"A", "Excellent - outstanding performance with only minor errors"},
        {"B", "Very good – above the average standard but with some errors" },
        {"C",  "Good– generally sound work with a number of notable errors" },
        {"D", "Satisfactory – fair but with significant shortcomings" },
        {"E", "Sufficient – performance meets the minimum criteria" },
        {"Fx", "Fail – some more work required before the credit can be awarded" },
        {"F", "Fail - considerable further work is required" }
    };

    bool inputIsG = false;

    Console.Write("Write your grade in [");
    foreach (var item in Grades)
    {
        Console.Write(" " + item.Key);
    }
    Console.Write(" ]\n Type \"G\" to exit\n");

    while (!inputIsG)
    {
        String input = Console.ReadLine();

        if (Grades.ContainsKey(input))
        {
            Console.WriteLine(Grades[input]);
        }
        else if (input == "G")
        {
            inputIsG = true;
        }
        else
        {
            Console.WriteLine("Invalid input, try again");
        }
    }
}

Console.WriteLine("Assignment 6");
ass6();
