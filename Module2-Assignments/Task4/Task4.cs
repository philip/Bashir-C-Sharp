﻿void ass4()
{
    double population = 6.5E12;
    int year = 2022;

    while (population < 10E12)
    {
        year++;
        population *= 1.014;
    }

    Console.WriteLine($"The year {year} the population will be {Math.Floor(population)}");
}

Console.WriteLine();
Console.WriteLine("Assignment 4");
ass4();