﻿void numToArr(int input, ref List<int> arr)
{
    arr.Add(input);
}

void ass7()
{
    List<int> nums = new List<int>();

    int runs = 0, input;

    do
    {
        if (runs == 0)
            Console.Write("Type a number: ");
        else
            Console.Write("Type another number: ");

        input = Helpers.getInputNumber();
        if (input == 0)
            continue;
        runs++;
        numToArr(input, ref nums);

    } while (input != 0);

    if (runs > 0)
        Console.WriteLine($"The smallest number you entered was {nums.Min()}");
    else
        Console.WriteLine("List is empty");
}

Console.WriteLine("Assignment 7");
ass7();