﻿static void ass1()
{
    const int spacing = 10;

    Console.WriteLine($"{"Number",-spacing}" +
                      $"{"Square",-spacing}" +
                      $"{"Cube",-spacing}");

    for (int i = 1; i <= 10; i++)
    {
        Console.WriteLine($"{i,-spacing}" +
                          $"{Math.Pow(i, 2),-spacing}" +
                          $"{Math.Pow(i, 3),-spacing}");
    }

}

Console.WriteLine("Assignment 1");
ass1();
