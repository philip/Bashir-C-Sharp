﻿void ass5()
{
    int celsiusToFahrenheit(int celsius)
    {
        return (9 / 5) * celsius + 32;
    }

    const int spacing = 10;

    Console.WriteLine($"{"Number",-spacing}" +
                      $"{"Fahrenheit",spacing}");

    for (int i = -30; i <= 50; i += 5)
    {

        Console.WriteLine($"{i,-spacing + 4}" +
                          $"{celsiusToFahrenheit(i),spacing}");
    }
}

Console.WriteLine();
Console.WriteLine("Assignment 5");
ass5();
