﻿static void ass2()
{
    void calculateSphereVolume(int radius)
    {
        for (int i = 1; i <= radius; i++)
        {
            double volume = (4 / 3) * Math.PI * Math.Pow(i, 3);
            Console.WriteLine($"Sphere\'s volume with radius {i} is {volume}");
        }
    }

    Console.WriteLine("Enter a radius:");
    int radius = Helpers.getInputNumber();

    calculateSphereVolume(radius);
}

Console.WriteLine();
Console.WriteLine("Assignment 2");
ass2();