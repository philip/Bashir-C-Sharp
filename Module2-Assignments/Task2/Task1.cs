﻿static int getInputNumber()
{
    int numericInput = 0;
    bool validInput = false;
    string textInput;

    while (!validInput)
    {
        textInput = Console.ReadLine();
        try
        {
            numericInput = Int32.Parse(textInput);
            validInput = true;
            Console.WriteLine($"You have entered the number {textInput}");
        }
        catch (FormatException)
        {
            Console.WriteLine($"Unable to parse '{getInputNumber}' as a number");
        }
    }
    return numericInput;
}

static void ass1()
{
    const int spacing = 10;

    Console.WriteLine($"{"Number",-spacing}" +
                      $"{"Square",-spacing}" +
                      $"{"Cube",-spacing}");

    for (int i = 1; i <= 10; i++)
    {
        Console.WriteLine($"{i,-spacing}" +
                          $"{Math.Pow(i, 2),-spacing}" +
                          $"{Math.Pow(i, 3),-spacing}");
    }

}

static void ass2()
{
    void calculateSphereVolume(int radius)
    {
        for (int i = 0; i <= radius; i++)
        {
            double volume = (4 / 3) * Math.PI * Math.Pow(i, 3);
            Console.WriteLine($"Sphere\'s volume with radius {i} is {volume}");
        }
    }

    Console.WriteLine("Enter a radius:");
    int radius = getInputNumber();

    calculateSphereVolume(radius);
}

static void ass3()
{
    Console.WriteLine("Enter a number between 2 and 9 for square creation");
    bool correntSize = false;
    int squareSize = 0;

    while (!correntSize)
    {
        squareSize = getInputNumber();
        correntSize = squareSize >= 2 && squareSize <= 9;
        Console.WriteLine(correntSize ? $"You have entered size {squareSize}" : $"Size out of bound, try again");
    }


    for (int i = 0; i < squareSize; i++)
    {
        for (int j = 0; j < squareSize; j++)
        {
            Console.Write("*");
        }
        Console.Write("\n");
    }
}

void ass4()
{
    double population = 6.5E9;
    int year = 2022;

    while (population < 10E9)
    {
        year++;
        population *= 1.014;
    }

    Console.WriteLine($"The year {year} the population will be {population}");
}

void ass5()
{
    int celsiusToFahrenheit(int celsius)
    {
        return (9 / 5) * celsius + 32;
    }

    const int spacing = 10;

    Console.WriteLine($"{"Number",-spacing}" +
                      $"{"Fahrenheit",spacing}");

    for (int i = -30; i <= 50; i += 5)
    {

        Console.WriteLine($"{i,-spacing + 4}" +
                          $"{celsiusToFahrenheit(i),spacing}");
    }
}

Console.WriteLine("Assignment 1");
ass1();

Console.WriteLine();
Console.WriteLine("Assignment 2");
ass2();

Console.WriteLine();
Console.WriteLine("Assignment 3");
ass3();

Console.WriteLine();
Console.WriteLine("Assignment 4");
ass4();

Console.WriteLine();
Console.WriteLine("Assignment 5");
ass5();




